clear all
format long
tic
% PROGRAMA PER MOSTRAR UNA ESCALA DEPENENT DE LA LATITUD PER A LA PROJECCI�
% CIL�NDRICA EQUIDISTANT. CAS PER A MART:
% https://spatialreference.org/ref/iau2000/49900/
a=3396190;
b=3376200;
R=(a+b)/2;
% Projecci� projecci� equirectangular
% https://en.wikipedia.org/wiki/Equirectangular_projection
syms phi;
syms lambda;
x=R*lambda;
y=R*phi;

% Derivades parcials
syms xp;
syms xl;
syms yp;
syms yl;

xp=diff(x,phi);
xl=diff(x,lambda);
yp=diff(y,phi);
yl=diff(y,lambda);

syms dphi;
syms dlamb;

% Coeficient de deformaci� k1
num=sqrt((xp^2+yp^2)*dphi^2+(xl^2+yl^2)*dlamb^2+2*(xp*xl+yp*yl)*dphi*dlamb);
den=sqrt(R^2*dphi^2+R^2*cos(phi)^2*dlamb^2);
k1=num/den;

% Fem els dibuixos al llarg de paral�lels.
% Par�metres previs:
latmax=60/180*pi; % Latitud m�xima de la gr�fica. El HiRISE recomana gastar 60�.
latmin=0; % Latitud m�nima de la gr�fica.

k1=subs(k1,dphi,0);
k1=subs(k1,dlamb,1);

% Corbes a la dreta
k11=@(phi) eval(k1);
k12=@(phi) (1/2*eval(k1));

% Corbes a l'esquerra
k14=@(phi) (-1*eval(k1));
k15=@(phi) (-1/2*eval(k1));

eix=@(x) x;
fig=figure;
hold on
% Dibuixem corbes
fplot(k11,eix,[latmin,latmax],'k')
fplot(k12,eix,[latmin,latmax],'k')
fplot(k14,eix,[latmin,latmax],'k')
fplot(k15,eix,[latmin,latmax],'k')
fplot(0,eix,[latmin,latmax],'k')

% Dibuixem rectes horitzontals
l0=0;
l1=20/180*pi;
l2=40/180*pi;
l3=60/180*pi;
plot([-k11(l0) k11(l0)], [l0 l0], 'k')
plot([-k11(l1) k11(l1)], [l1 l1], 'k')
plot([-k11(l2) k11(l2)], [l2 l2], 'k')
plot([-k11(l3) k11(l3)], [l3 l3], 'k')

axis([-2 2 0 latmax])
set(gca,'XColor', 'none','YColor','none')
hold off
toc