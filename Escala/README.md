# Generació d'escala gràfica
Este programa de MatLab servix per generar una escala gràfica depenent de la latitud per a la projecció cilíndrica equidistant.

![](Escala/imgs/f1.png "Equiàrea")

### Referències
1. Projecció cilíndrica equidistant, [Wikipedia](https://en.wikipedia.org/wiki/Equirectangular_projection).
2. Fundamentos de cartografía matemática, Sergio Baselga, Editorial UPV.
