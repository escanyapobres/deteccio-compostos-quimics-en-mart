# Animacions virtuals de vols en Mart
## Descripció
En esta carpeta es troben els enllaços als vols virtuals d'alguns llocs de Mart. Estos vídeos han estat fets amb Google Earth Studio i postprocessats amb el codi de Python ací adjunt.

## Vídeos
Cliqueu en les imatges per vore els vídeos.
* Airy-0:

[![Airy-0](http://img.youtube.com/vi/du1GEJtUNUc/0.jpg)](http://www.youtube.com/watch?v=du1GEJtUNUc "Airy-0")

* Curiosity al cràter de Gale:

[![Curiosity](http://img.youtube.com/vi/2vBD-3vvfaw/0.jpg)](http://www.youtube.com/watch?v=2vBD-3vvfaw "Curiosity")

## Llibreries
Les dependències del codi adjunt són:
* opencv
* numpy
* os (integrada)

```python
import cv2
import numpy as np
import os
from os.path import isfile, join
```

## Referències
1. Cartografia de la NASA visualitzada amb Google Earth Studio.
2. [Convert images to videos using Python.](https://medium.com/@iKhushPatel/convert-video-to-images-images-to-video-using-opencv-python-db27a128a481)
