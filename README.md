# Documentació complementària

Este repositori de GitLab conté informació complementària al meu treball de fi de grau:
*  Projeccions: Exemples de projeccions.
*  Animacions: Cartografia de Mart.
*  Escala gràfica: Per a l'escala cilíndrica equidistant.


*Ausiàs Roch Talens*