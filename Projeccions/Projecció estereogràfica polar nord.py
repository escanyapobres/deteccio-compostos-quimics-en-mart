# Llibreries
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap

# Generem el mapa base
fig = plt.figure(1,frameon=False)
m = Basemap(projection = 'npstere', boundinglat = 0, lon_0 = 0, resolution = 'l', round = True)

# Grossor dels meridians i els paral·lels
gl = 0.3

# Altres paràmetres del mapa
m.drawcoastlines(linewidth = 0.4)
#m.drawcountries(linewidth = 0.7)
m.drawparallels(np.arange(-90, 90, 30), linewidth = gl)
m.drawmeridians(np.arange(-180, 180, 30), linewidth = gl)

# Extrems del mapa circulars
cercle = m.drawmapboundary(linewidth = gl, color ='w')
cercle.set_clip_on(False)

# Color dels continents
m.fillcontinents(color = 'lightgray')

# Guardem la imatge en .png o .svg
fig.savefig("PSPN.png", dpi = 600)

# Mostrar
plt.show()

# Referències:
# https://matplotlib.org/basemap/api/basemap_api.html
# https://stackoverflow.com/questions/51394926/basemap-round-stereographic-plot-mapboundary-cropped
# https://matplotlib.org/basemap/users/graticule.html