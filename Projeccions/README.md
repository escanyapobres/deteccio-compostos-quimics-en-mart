# Previsualitzacions de projeccions
## Resum
Arxius per la generació de previsualitzacions de projeccions en python.
## Resultats
Resultats de lesprevisualitzacions:

*  **Projecció equiàrea**:
![](Projeccions/imgs/PEA.png "Equiàrea")
*  **Projecció estereogràfica polar nord**:
![](Projeccions/imgs/PSPN.png "Estereogràfica")
*  **Projecció estereogràfica polard sud**:
![](Projeccions/imgs/PSPS.png "Estereogràfica")

### Referències
1.  Documentació: https://matplotlib.org/basemap/api/basemap_api.html
2.  Mapes redons: https://stackoverflow.com/questions/51394926/basemap-round-stereographic-plot-mapboundary-cropped
3.  Retícula: https://matplotlib.org/basemap/users/graticule.html